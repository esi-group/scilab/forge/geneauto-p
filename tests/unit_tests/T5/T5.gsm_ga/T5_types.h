
#ifndef __T5_types__
#define __T5_types__

/* Includes */

#include "GATypes.h"


/* Type declarations */

typedef struct {
    REAL Counter_6;
} t_Counter_io;

typedef struct {
    REAL B22;
    REAL B23;
    REAL B24;
    REAL B25;
    REAL B26;
    REAL B27;
    REAL B28;
    REAL B29;
} t_T5_io;

typedef struct {
    /*  Block: -26d26ed8:137ef86bff0:-7e9c  */
    INT32 Counter_3_memory;
} t_T5_state;



#endif
