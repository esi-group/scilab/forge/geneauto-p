function perform()

    loadXcosLibs();
    macros=libraryinfo('ga_xml_block_tranlib')
    interfunc=strsubst(macros,"_tran","");
    
    // ignored blocks
    ignored=["INPUTPORT" "OUTPUTPORT"]
    
    for i=1:size(macros, '*')
        m=macros(i);
        f=interfunc(i);
        
        // ignore some blocks which need a parent diagram to perform
        if or(f == ignored) then
            continue
        end
        
        execstr("o="+f+"(''define'');");
        [name,labels,values,EXPRS,ok]=block_tran(o);
        assert_checktrue(ok);
        
        // disp(f+" is mapped to "+name);
    end
    
endfunction
perform();
