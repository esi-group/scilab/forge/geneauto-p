#include "scicos_block4.h"
#include <machine.h>
#include <math.h>

void logblkz_m(scicos_block *block,int flag)
{
  double *ur,*ui;
  double *yr,*yi;
  double *rpar;
  int nu,mu,i;

  mu=GetInPortRows(block,1);
  nu=GetInPortCols(block,1);
  ur=GetRealInPortPtrs(block,1);
  ui=GetImagInPortPtrs(block,1);
  yr=GetRealOutPortPtrs(block,1);
  yi=GetImagOutPortPtrs(block,1);
  rpar=GetRparPtrs(block);
  if ((flag==1)|(flag>=4)) {
  for(i=0;i<mu*nu;i++) 
     {yr[i]=log(sqrt((ur[i]*ur[i]+ui[i]*ui[i])))/log(*rpar);
     yi[i]=atan(ui[i]/ur[i])/log(*rpar);}
  }
}
