#include "scicos_block4.h"
#include <math.h>

void logblk_m(scicos_block *block,int flag)
{
  double *u;
  double *y;
  double *rpar;
  int nu,mu,i;

  mu=GetInPortRows(block,1);
  nu=GetInPortCols(block,1);
  u=GetRealInPortPtrs(block,1);
  y=GetRealOutPortPtrs(block,1);
  rpar=GetRparPtrs(block);

  if (flag==1) {
    for(i=0;i<mu*nu;i++) {
       if (u[i] > 0)   
        {y[i]=log(u[i])/log(*rpar);}
       else
        {flag=-2;
         return;}}
  }
   else if (flag==6) 
     {for(i=0;i<mu*nu;i++) {
       if (u[i] > 0)   
       {y[i]=log(u[i])/log(*rpar);}}} 
}
