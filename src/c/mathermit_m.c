#include "scicos_block4.h"
#include <machine.h>

static int mtran_(double *a, int *na, double *b, 
	int *nb, int *m, int *n);

void mathermit_m(scicos_block *block,int flag)
{
  double *ur;
  double *yr;
  double *ui;
  double *yi;
  int nu,mu,i;

  nu=GetInPortRows(block,1);
  mu=GetInPortCols(block,1);

  ur=GetRealInPortPtrs(block,1);
  ui=GetImagInPortPtrs(block,1);
  yr=GetRealOutPortPtrs(block,1);
  yi=GetImagOutPortPtrs(block,1);
  mtran_(ur,&nu,yr,&mu,&nu,&mu);
  mtran_(ui,&nu,yi,&mu,&nu,&mu);
  for(i=0;i<mu*nu;i++) *(yi+i)=-(*(yi+i));
}

static int mtran_(double *a, int *na, double *b, 
	int *nb, int *m, int *n)
{
    /* System generated locals */
    int i__1, i__2;

    /* Local variables */
    int i__, j, ia, ib;

    /* Parameter adjustments */
    --b;
    --a;

    /* Function Body */
    ia = 0;
    i__1 = *n;
    for (j = 1; j <= i__1; ++j) {
	ib = j;
	i__2 = *m;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    b[ib] = a[ia + i__];
	    ib += *nb;
/* L10: */
	}
	ia += *na;
/* L20: */
    }
    return 0;
}

