#include "scicos_block4.h"
#include <math.h>
void invblk_m(scicos_block *block,int flag)
{
  if ((flag==1)|(flag==6)) {
  double *u;
  double *y;

  int nu,mu,i;
  mu=GetOutPortRows(block,1);
  nu=GetOutPortCols(block,1);
  u=GetRealInPortPtrs(block,1);
  y=GetRealOutPortPtrs(block,1);
  for (i=0;i<mu*nu;i++) 
	{if (u[i]!=0) { 
            *(y+i)=1/u[i];}
         else if (flag!=6) {
             set_block_error(-2);
             return;}}
}
}
