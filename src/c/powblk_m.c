#include "scicos_block4.h"
#include <math.h>
void powblk_m(scicos_block *block,int flag)
{
  if (flag==1) {
  double *u;
  double *y;
  double *rpar;

  int nu,mu,i,nin;
  nin=GetNin(block);
  mu=GetOutPortRows(block,1);
  nu=GetOutPortCols(block,1);
  u=GetRealInPortPtrs(block,1);
  y=GetRealOutPortPtrs(block,1);
  if (nin==1) rpar=GetRparPtrs(block);
  else rpar=GetRealInPortPtrs(block,2);
  for (i=0;i<mu*nu;i++) 
	{*(y+i)=pow(*(u+i),*rpar);}}
}

