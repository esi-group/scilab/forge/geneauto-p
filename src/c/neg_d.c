#include <math.h>
#include "scicos_block4.h"
#define neg(y1,u1,mu,nu)  {for (i=0;i<mu*nu;i++)\
	                      {y1[i]=-u1[i];\
	                      }}
void neg_d(scicos_block *block,int flag)
{
 if (flag==1){
  int nu,mu,i,ut;
  ut=GetInType(block,1);
  mu=GetOutPortRows(block,1);
  nu=GetOutPortCols(block,1);
  switch (ut)
  {
      case SCSREAL_N :{
           double *u1,*y1;
           u1=GetRealInPortPtrs(block,1);
           y1=GetRealOutPortPtrs(block,1);
           neg(y1,u1,mu,nu);
           break;}

      case SCSINT32_N :{
           long *u1,*y1;
           u1=Getint32InPortPtrs(block,1);
           y1=Getint32OutPortPtrs(block,1);
           neg(y1,u1,mu,nu);
           break;}

      case SCSINT16_N :{
           short *u1,*y1;
           u1=Getint16InPortPtrs(block,1);
           y1=Getint16OutPortPtrs(block,1);
           neg(y1,u1,mu,nu);
           break;}

      case SCSINT8_N :{
           char *u1,*y1;
           u1=Getint8InPortPtrs(block,1);
           y1=Getint8OutPortPtrs(block,1);
           neg(y1,u1,mu,nu);
           break;}

      case SCSUINT32_N :{
           unsigned long *u1,*y1;
           u1=Getuint32InPortPtrs(block,1);
           y1=Getuint32OutPortPtrs(block,1);
           neg(y1,u1,mu,nu);
           break;}

      case SCSUINT16_N :{
           unsigned short *u1,*y1;
           u1=Getuint16InPortPtrs(block,1);
           y1=Getuint16OutPortPtrs(block,1);
           neg(y1,u1,mu,nu);
           break;}

      case SCSUINT8_N :{
           unsigned char *u1,*y1;
           u1=Getuint8InPortPtrs(block,1);
           y1=Getuint8OutPortPtrs(block,1);
           neg(y1,u1,mu,nu);
           break;}

      case SCSCOMPLEX_N :{
           double *u1r,*y1r;
           double *u1i,*y1i;
           u1r=GetRealInPortPtrs(block,1);
           y1r=GetRealOutPortPtrs(block,1);
           u1i=GetImagInPortPtrs(block,1);
           y1i=GetImagOutPortPtrs(block,1);
           for (i=0;i<mu*nu;i++) 
	       {y1r[i]=-u1r[i];
                y1i[i]=-u1i[i];}
	   break;}

      default :{
          set_block_error(-4); 
	  return;} 
   } 
  }

}
