// This file is released under the 3-clause BSD license. See COPYING-BSD.

function buildmacros()
  macros_path = get_absolute_file_path("buildmacros.sce");

  subdirs = ["cosf_generator" "xml_generator" "GeneAutoPal"];
  directories = pathconvert(macros_path+"/"+subdirs,%F);
  for i=1:size(directories,"*") do
    if ~isdir(directories(i)) then
        error(msprintf(gettext("%s: The directory ''%s'' doesn''t exist or is not read accessible.\n"),"buildmacros",directories(i)));
    end
  end
  tbx_builder(pathconvert(directories+"/buildmacros.sce",%F));
endfunction

buildmacros();
clear buildmacros; // remove buildmacros on stack

