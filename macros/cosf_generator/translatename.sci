function sci_name=translatename(ga_name)
  matrixname=['Product','MATMUL';..
              'Div','Div';..
              'Neg','Neg';..
              'Gain','GAINBLK';..
              'UnitDelay','DOLLAR_m';..
              'DelayRE','DelayRE';..
              'Logic','LOGICAL_OP';..
              'Bitwise Operator','LOGICAL_OP';..
              'Bitwise Not','LOGICAL_OP';..
              'Pulse1','PULSE';..
              'Pulse2','PULSE';..
              'SampleAndHold','SampleAndHold';..
              'BascR','BascR';..
              'BascS','BascS';..
              'Abs','ABS_VALUE';..
              'LIM','LIM';..
              'Max','MAXMIN';..
              'Min','MAXMIN';..
              'Sign','SIGNUM';..
              'Math','MathFunc';..
              'Switch','SWITCH2_m';..
              'Multi Port Switch','M_SWITCH';..
              'Sum','SUMMATION';..
              'For Iterator','';..
              'Bus Creator','';..
              'Bus Selector','';..
              'Mux','MUX';..
              'Demux','DEMUX';..
              'Ground','Ground_g';..
              'Terminator','Terminator';..
              'Switch case','';..
              'Constant','CONST_m';..
              'F','F_g';..
              'G','G_g';..
              'Read Map Array','';..
              'Write Map Array','';..
              'If','IFTHEL_f';..
              'If Then Else','';..
              'Sequencer','';..
              'From Data','';..
              'Goto Data','';..
              'From','FROM';..
              'Goto','GOTO';..
              'To Workspace','';..
              'From Workspace','';..
              'Data Conversion','CONVERT';..
              'Trigonometric','TrigFun';..
              'GotoTagVisibility','GotoTagVisibility';..
              'Inport','IN_f';..
              'Outport','OUT_f';..
              'RelationalOperator','RELATIONALOP';..
	      'SubSystem','SUPER_f';..
	      'Scope','CSCOPE';..
	      'ActionPort','CLKINV_f';..
              ]
   sci_name=matrixname(find(matrixname(:,1)==ga_name),2)
endfunction

