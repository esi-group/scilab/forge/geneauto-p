function block_define=Max_cosf(block_define,par_name,par_exprs,par_val)
  k=find(par_name=='Input');
  block_define.graphics.exprs=[sci2exp(2) par_exprs(k) sci2exp(1)];
  getvalue=setvalue;
  %scicos_prob=%f;
  block_define=MAXMIN("set",block_define);
endfunction
