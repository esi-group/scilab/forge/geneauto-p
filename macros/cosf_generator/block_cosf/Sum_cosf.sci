function block_define=Sum_cosf(block_define,par_name,par_exprs,par_val)
  k=find(par_name=='Inputs')
  vec2=[]
  vec=strsplit(par_exprs(k),1:1:length(par_exprs(k))-1);  
  for i=1:size(vec,'*')
    if vec(i)=='+' then
      vec2=[vec2,1]
    elseif vec(i)=='-' then
      vec2=[vec2,-1]
    end
  end
  block_define.graphics.exprs(2)=sci2exp(vec2);
  block_define.graphics.exprs(1)=sci2exp(block_define.model.outtyp)
  block_define.graphics.exprs(3)=sci2exp(0);
  getvalue=setvalue;
  %scicos_prob=%f;
  block_define=SUMMATION("set",block_define)
endfunction
