function block_define=Gain_cosf(block_define,par_name,par_exprs,par_val)
  k=find(par_name=='Gain');
  block_define.graphics.exprs(1)=par_exprs(k);
  //execstr(par_exp(k)+'='+par_val(k));
  getvalue=setvalue;
  %scicos_prob=%f;
  block_define=GAINBLK("set",block_define);
endfunction

