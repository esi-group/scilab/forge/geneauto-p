function block_define=LIM_cosf(block_define,par_name,par_exprs,par_val)
  xtrn=find(par_name=='EXTH1')
  if par_exprs(xtrn)=='off' then
     block_define.model.ipar=2
  else
     block_define.model.ipar=1
     k=find(par_name=='PARH1')
     kk=find(par_name=='PARH2')
     block_define.graphics.exprs=[par_exprs(k);..
                                  par_exprs(kk)];
  end
  getvalue=setvalue;
  %scicos_prob=%f;
  block_define=LIM("set",block_define)
endfunction
