function block_define=Pulse2_cosf(block_define,par_name,par_exprs,par_val)
  xtrn=find(par_name=='EXTH1')
  if par_exprs(xtrn)=='off' then
     block_define.model.rpar=2
  else
     block_define.model.rpar=1
     k=find(par_name=='PARH1')
     kk=find(par_name=='PARH2')
     block_define.graphics.exprs=[sci2exp(find(oper==par_exprs(k))-1);..
                                  sci2exp(find(oper==par_exprs(kk))-1);..
				  sci2exp(2)];
  end
  getvalue=setvalue;
  %scicos_prob=%f;
  block_define=PULSE("set",block_define);
endfunction
