function block_define=RelationalOperator_cosf(block_define,par_name,par_exprs,par_val)
  k=find(par_name=='Operator')
  operator=['==','~=','<','<=','>','>=']
  opnum=find(operator==par_exprs(k))
  block_define.graphics.exprs=[sci2exp(opnum-1);sci2exp(0);sci2exp(block_define.model.outtyp)]
  getvalue=setvalue
  %scicos_prob=%f
  block_define=RELATIONALOP("set",block_define)
endfunction
