function block_define=Inport_cosf(block_define,par_name,par_exprs,par_val)
  k=find(par_name=='Port')
  block_define.graphics.exprs(1)=par_exprs(k);
  getvalue=setvalue;
  %scicos_prob=%f
  block_define=IN_f("set",block_define);
endfunction
