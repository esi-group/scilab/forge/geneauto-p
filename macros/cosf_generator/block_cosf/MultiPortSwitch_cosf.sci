function block_define=MultiPortSwitch(block_define,par_name,par_exprs,par_val)
  k=find(par_name=='Input')
  block_define.graphics.exprs(1)=par_exprs(k);
  getvalue=setvalue;
  %scicos_prob=%f;
  block_define=M_SWITCH("set",block_define)
endfunction
