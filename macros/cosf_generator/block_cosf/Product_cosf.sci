function block_define=Product_cosf(block_define,par_name,par_exprs,par_val)
ind=find(par_name=='Multiplication')
if par_exprs(ind)=='Element-wise(.*)' then
  block_define.graphics.exprs(2)='2';
else
  block_define.graphics.exprs(2)='1';
end
getvalue=setvalue;
%scicos_prob=%f;
block_define=MATMUL("set",block_define)
endfunction
