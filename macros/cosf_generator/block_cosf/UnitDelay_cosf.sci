function block_define=UnitDelay_cosf(block_define,par_name,par_exprs,par_val)
  k=find(par_name=='InitialValue');
  block_define.graphics.exprs(1)=par_exprs(k);
  k=find(par_name=='SampleTime')
  if evstr(par_exprs(k))==-1 then
     block_define.graphics.exprs(2)='1'
  else
     block_define.graphics.exprs(2)='0'
  end
  //execstr(par_exprs(k)+'='+par_val(k));
  getvalue=setvalue;
  %scicos_prob=%f;
  block_define=DOLLAR_m("set",block_define);
endfunction

