// Convert binary operators to Gene-Auto XML
  
// Authors : Fady Nassif, Serge Steer. INRIA. 2007-2009. GPL Version 3
// Contributors : Daniel Tuulik. IB Krates. 2011

// Copyright (C) INRIA 2007-2009
// Copyright (C) IB Krates 2011

function [txt,ok]=binaryops2xml(operands,op)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  opstable=[
		'+' 'ADD_OPERATOR' "0" "12"
		'-' 'SUB_OPERATOR' "1" "12"
		'.*' 'MUL_OPERATOR' "2" "13"
		'/' 'DIV_OPERATOR' "3" "13"
		'^' 'POWER_OPERATOR' "4" "13"
		'*' 'MUL_MAT_OPERATOR' "5" "13"
		'==' 'EQ_OPERATOR' "12" "10"
		'>=' 'GE_OPERATOR' "13" "10"
		'>' 'GT_OPERATOR' "14" "10"
		'<=' 'LE_OPERATOR'  "15" "10"
		'<' 'LT_OPERATOR' "16" "10"
		'~=' 'NE_OPERATOR' "17" "9"
		'&'  'LOGICAL_AND_OPERATOR' "18","5"
		'|'  'LOGICAL_OR_OPERATOR' "19","4"];
  k=find(opstable(:,1)==op)
  if k==[] then
    txt=[];ok=%f
    errormessage('Operator ""'+operator+'"" is not yet supported")
  end
	    
  idstart=getxmlid()
  
  id=newxmlid()
  [xmlop1,ok]=ga_expressiontree2xml(operands(1))
  if ~ok then txt=[];setxmlid(idstart),return,end
  [xmlop2,ok]=ga_expressiontree2xml(operands(2))
  if ~ok then txt=[];setxmlid(idstart),return,end
    
  txt=['<BinaryExpression '+id+'>'
       '    <leftArgument type=""gaxml:object"">'
       '        '+xmlop1
       '    </leftArgument>'
       '    <rightArgument type=""gaxml:object"">'
       '        '+xmlop2
       '    </rightArgument>'
       '    <operator type=""gaxml:object"">'
       '        <BinaryOperator name=""'+opstable(k,2)+'"" ordinal=""'+opstable(k,3)+'"" precedence=""'+opstable(k,4)+'"" type=""gaxml:enum""/>'
       '    </operator>'
       '</BinaryExpression>']
endfunction
