function [txt,ok]=Scalar2xml(value)
  // Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  select type(value)
  case 10 then
    [txt,ok]=String2xml(value)
  case 8 then
    [txt,ok]=Integer2xml(value)
  case 1 then
    [txt,ok]=Real2xml(value)
  else
    txt=[];ok=%f
    errormessage('Variable with Scilab type '+string(type(value))+' are not yet handled')
  end
endfunction
