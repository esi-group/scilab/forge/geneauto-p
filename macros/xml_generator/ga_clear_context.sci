function ga_clear_context()
  // Author : Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  clearglobal ksel //for error reporting
  clearglobal scs_msav
endfunction
