function Pos=ga_block_pos(o)
// Translate the position of the block o into Simulink coordinates
  // Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  G=o.graphics
  O=G.orig;sz=G.sz;
  O=O(:)-[rect(1);rect(2)]
  O(1)=max(O(1),0);O(2)=max(O(2),0);
  Pos=round([O(1),H-O(2)-sz(2),O(1)+sz(1),H-O(2)])
endfunction
      
