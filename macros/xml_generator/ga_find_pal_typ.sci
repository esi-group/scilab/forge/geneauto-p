function pal_typ=ga_find_pal_typ(blktyp)
// Author : Fady Nassif, Copyright INRIA, GPL Version 3, 29 June 2007
  all_blocks=['Product','Combinatorial';
              'Div','Combinatorial';
              'Neg','Combinatorial';
              'Gain','Combinatorial';
              'UnitDelay','Sequential';
              'DelayRE','Combinatorial';
              'Logic','Combinatorial';
              'Bitwise Operator','Combinatorial';
              'Bitwise Not','Combinatorial';
              'Pulse1','Sequential';
              'Pulse2','Sequential';
              'SampleAndHold','Combinatorial';
              'BascR','Sequential';
              'BASCR','Sequential';
              'BascS','Sequential';
              'BASCS','Sequential';
              'Abs','Combinatorial';
              'LIM','Combinatorial';
	      'MAXMIN','Combinatorial';
              'Max','Combinatorial';
	      'Min','Combinatorial';
              'Sign','Combinatorial';
              'Math','Combinatorial';
              'Switch','Combinatorial';
              'Multi Port Switch','Combinatorial';
              'Sum','Combinatorial';
              'For Iterator','Control';
              'Bus Creator','Combinatorial';
              'Bus Selector','Combinatorial';
              'Mux','Combinatorial';
              'Demux','Combinatorial';
              'Ground','Source';
              'Terminator','Sink';
              'Switch case','Subsystem';
              'Constant','Source';
              'F','Combinatorial';
              'G','Combinatorial';
              'Read Map Array','Sequential';
              'Write Map Array','Sequential';
              'If','Combinatorial';
              'If Then Else','Combinatorial';
              'Sequencer','Control';
              'From Data','Source';
              'Goto Data','Sink';
              'From','Routing';
              'Goto','Routing';
              'To Workspace','';
              'From Workspace','';
              'DataTypeConversion','Combinatorial';
              'Trigonometric','TrigFun';
              'GotoTagVisibility','Routing';
              'Inport','Source';
              'Outport','Sink';
              'RelationalOperator','Combinatorial';
	      'SubSystem','Subsystem';
	      'Scope','Sink';
	      'StepGenerator','Source';
              'SinusGenerator','Source';
	      'ActionPort','Control'
              ]
  pal_typ=all_blocks(find(all_blocks(:,1)==blktyp),2)
endfunction

