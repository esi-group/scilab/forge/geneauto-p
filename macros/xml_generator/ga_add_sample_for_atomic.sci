function freof=ga_add_sample_for_atomic(o,scs_m)
// Author : Fady Nassif, Copyright INRIA, GPL Version 3, 29 June 2007
  freof=[];
  if o.model.sim(1)=='asuper' then
    if or(o.graphics.pein<>[0;[]]) then
      lnk=scs_m.objs(o.graphics.pein)
      fromb=scs_m.objs(lnk.from(1))
      while fromb.gui=="CLKSPLIT_f" do
	lnk=scs_m.objs(fromb.graphics.pein);
	fromb=scs_m.objs(ln.from(1));
      end
      if fromb.gui=='SampleCLK' then
	freof=fromb.model.rpar
      end
    end
  end
endfunction
