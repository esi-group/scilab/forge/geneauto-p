function [ok,scs_m]=ga_plug_scicos_block(scs_m,selblocknumber,dest,guiname,freof)
//Plug the Scicos block generated into the main diagram for checking

// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  
// Make scilab know the newly created block
//-----------------------------------------
// - First unlink the previously linked simulation routine if any
  cname=strsubst(guiname,'_gagui','_blk')
  [t,ilib]=c_link(cname)
  if t then ulink(ilib),end
 
  // - Compile the C codes of the simulation routines and build a dynamic library
 
  if  execstr('exec(dest+''/builder.sce'',-1);','errcatch')<>0 then
    message(['Generated block building failed:';lasterror()]),
    ok=%f
    return,
  end
  
  // - Load the dynamic library and Scilab functions
  if  execstr('exec(dest+''/loader.sce'',-1);','errcatch')<>0 then
    message(['Generated block loading failed:';lasterror()]),
    ok=%f
    return,
  end


  //create or update the new block on the Scicos window
  //---------------------------------------------------
  //the superblock which has been translated
  selblk=scs_m.objs(selblocknumber); 
  //the generated block 
  execstr('ga_block='+guiname+'(''define'')') 

  // - Check if the block already exist in the Scicos Diagram
  ksk=[]
  nobjs=size(scs_m.objs)
  
  for i=1:nobjs
    if (typeof(scs_m.objs(i))=='Block')&(scs_m.objs(i).gui==ga_block.gui) then
      ksk=[ksk,i]
    end
  end
   //- The block does not exist, create it
  if ksk==[] then 
    //- The block does not exist, create it
    // Set generated block size equal to superblock size
    ga_block.graphics.sz=selblk.graphics.sz;
    
    if freof <>[] then //external SampleCLK required
      
      //Add a SampleCLK block to drive the GeneAuto generated block
      clk=SampleCLK('define');clk.graphics.sz=[60 40];
      //Set the position of the ga_block and clk block
      clk.graphics.orig=[selblk.graphics.orig(1)+(selblk.graphics.sz(1)-clk.graphics.sz(1))/2;
		    selblk.graphics.orig(2)-clk.graphics.sz(2)*1.3]';
      clk.model.rpar=freof;
      clk.graphics.peout=nobjs+3;
      clk.graphics.exprs=[sci2exp(freof(1)) sci2exp(freof(2))]
      ga_block.graphics.orig=[selblk.graphics.orig(1), ...
		    clk.graphics.orig(2)-ga_block.graphics.sz(2)*1.3];
      ga_block.graphics.pein=[zeros(1,size(selblk.graphics.pein,'*'));nobjs+3];
      ga_block.model.evtin=[ones(1,size(selblk.graphics.pein,'*'));ga_block.model.evtin];
    //add a link connecting the event output of clk  to the event input of ga_block
      xl=clk.graphics.orig(1)+clk.graphics.sz(1)*ones(2,1)/2;
      yl=[clk.graphics.orig(2);
	  ga_block.graphics.orig(2)+ga_block.graphics.sz(2)];
      typ=-1;
      clr=default_color(typ); 

      lk=scicos_link(xx=xl,yy=yl,ct=[clr,typ],..
				      from=[nobjs+1 1 0],..
				      to=[nobjs+2,size(ga_block.model.evtin,'*'),1]);
      scs_m.objs(nobjs+1)=clk;
      scs_m.objs(nobjs+2)=ga_block;
      scs_m.objs(nobjs+3)=lk
    else // No SampleCLK required
      ga_block.graphics.orig=[selblk.graphics.orig(1),selblk.graphics.orig(2)-ga_block.graphics.sz(2)*1.3]
      ga_block.graphics.pein=zeros(1,size(selblk.graphics.pein,'*'));
      ga_block.model.evtin=ones(1,size(selblk.graphics.pein,'*'));
      scs_m.objs($+1)=ga_block
    end
  else 
    // - The block exists, update it
    for i=ksk
      ga_block.graphics.sz=scs_m.objs(i).graphics.sz
      ga_block.graphics.orig=[scs_m.objs(i).graphics.orig(1),scs_m.objs(i).graphics.orig(2)]
      if ((size (selblk.graphics.pein,'*')) == (size (scs_m.objs(i).graphics.pein))) then
        ga_block.graphics.pein=scs_m.objs(i).graphics.pein;
	ga_block.model.evtin= scs_m.objs(i).model.evtin;
      else
	if (freof == []) then
	  ga_block.graphics.pein=zeros(1,size(selblk.graphics.pein,'*'))
          ga_block.model.evtin=ones(1,size(selblk.graphics.pein,'*'));
	else
	  ga_block.graphics.pein=[zeros(1,size(selblk.graphics.pein,'*'));0]
	  ga_block.model.evtin=[ones(1,size(selblk.graphics.pein,'*'));1];
	end
      end
      scs_m = changeports(scs_m,list('objs',i), ga_block);
      if ((freof<>[]) & (scs_m.objs(i-1).gui(1)=="SampleCLK")) then
	scs_m.objs(i-1).model.rpar=freof;
	scs_m.objs(i-1).graphics.exprs=[sci2exp(freof(1)) sci2exp(freof(2))]
      end
    end
  end
  
  //return the gui function in the calling environment
  execstr(guiname+'=resume('+guiname+')')
endfunction
