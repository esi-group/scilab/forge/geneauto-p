function field=ga_xml_subst(field)
// Author : Fady Nassif, Copyright INRIA, GPL Version 3, 29 June 2007  
  field=strsubst(field,'&','&amp;');
  field=strsubst(field,'<','&lt;');
  field=strsubst(field,'>','&gt;');
  field=strsubst(field,'""','&quot;');
  field=strsubst(field,'''','&apos;');
endfunction
