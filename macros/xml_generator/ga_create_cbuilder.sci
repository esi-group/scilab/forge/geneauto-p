function  ok=ga_create_cbuilder(src_path,tit)
//Create the builder.sce file which will be used by scilab to build the
//dynamic library associated with the C code (Scicos simulation
//function, and generated code)
  
// Author : Fady Nassif,  Copyright INRIA, GPL Version 3, 29 June 2007  
  txt=['mode(-1)';
       'curdir=pwd();'
       'DIR=get_absolute_file_path(''builder.sce'');'
       'chdir(DIR)'
       'comp_fun_lst='+sci2exp(tit+'_blk');
       'c_prog_lst=listfiles(''*.c'');'
       'c_prog_lst(c_prog_lst==''main.c'')=[]';
       'prog_lst=strsubst(c_prog_lst,''.c'',''.o'');'
       'ilib_for_link(comp_fun_lst,prog_lst,[],''c'',''Makelib'',''loader.sce'','+sci2exp(tit)+')';
       'chdir(curdir)'
       'clear  DIR curdir get_absolute_file_path  comp_fun_lst c_prog_lst prog_lst ilib_for_link']
  ok=execstr('mputl(txt,src_path+''builder.sce'')','errcatch')==0
  if ~ok then message(lasterror()),end
endfunction
