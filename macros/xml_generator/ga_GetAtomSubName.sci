function AtomSub=ga_GetAtomSubName(scs_m,Bn)
// Author : Fady Nassif, Copyright INRIA, GPL Version 3, 29 June 2007
  AtomSub=[];
if argn(2)==1 then 
  Bn='B'
end
obj=scs_m.objs
for i=1:lstsize(obj)
  o=obj(i)
  if typeof(o)=='Block' then
    if or(o.model.sim(1)==['super','csuper','asuper']) then
      if o.model.sim(1)=='asuper' then 
        AtomSub=[AtomSub;Bn+string(i)]
      end
      bn=Bn+string(i)+'_' 
      atomSub=ga_GetAtomSubName(o.model.rpar,bn)
      AtomSub=[AtomSub;atomSub];
    end  
  end
end
endfunction
