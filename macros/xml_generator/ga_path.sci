function p=ga_path()
// Returns the path of the directory where the GeneAuto toolbox is
// located
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007  
 
    w=string(evstr(whereis('ga_path')));
    p=w(1); //
    k=strindex(p,['/','\'])
    p=part(p,1:k($-2))
  
endfunction
 
