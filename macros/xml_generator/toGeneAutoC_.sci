function toGeneAutoC_()

    FindSBParams=ga_FindSBParams; //use FindSBParams in xmlgenlib instead of the
                                  //standard one in scicoslib

   errcatch(-1)
   %pt = [];
   
   // Is there any selected super block?
   //----------------------------------
    
    if ~exists("blk") then
        // No selected block
        // Note: Currently only conversion from superblocks is supported, 
        // but we leave the message more general here 
        message("Please select a block!")
        clear FindSBParams;return
    end 
    
    if typeof(blk)<>'Block' then return,end // no selected block
    if and(blk.model.sim(1)<>['super','asuper']) then
       message("Code Generation only works for a Superblock!")
       clear FindSBParams;return
    end
    
    // Select generated files location
    //--------------------------------
    msg='Select a destination directory for the generated files '+..
	 'Warning existing files in this directory may be overwritten';
    dest=uigetdir(title=msg)
    if length(dest)==0 then return,end //canceled by user
    dest=pathconvert(dest,%t,%f)
    [status,mess]=mkdir(dest)
    if status==0 then message(mess),return,end

    // Analyse the super block and generate codes (generic and Scicos specific)
    // ------------------------------------------------------------------------
    
    // FIXME: for Xcos we only need to store the block uuid ?
    selblocknumber = [];
    ga_set_context(scs_m,selblocknumber) //for error reporting

    main_scs_m=ga_replaceports(blk.model.rpar),
    xml_path=[];
    tit=strsubst(main_scs_m.props.title(1),' ','')
    if length(tit) == 0 then
      tit="Untitled";
    end
    
    //------------------------------------
    // - Main directory
    [status,mess]=mkdir(dest);
    if status==0 then message(mess),ok=%f,return,end
 
    // - generated XML file directory
    xml_path=pathconvert(dest,%t,%t)
    [status,mess]=mkdir(xml_path);
    if status==0 then message(mess),ok=%f,return,end
    
    // Analyse main_scs_m superbloc data structure and generate it's XML model
    //-----------------------------------------------------------------------
    [ok,xml,capt,actt,freof]=ga_scicos2xml(main_scs_m)
    if ~ok then return;end
 
    // - write the xml model
    filename=tit+'.gsm.xml';
    xml_file=xml_path+filename;
    mputl(xml,xml_file);

    mprintf('The XML file ' + filename + ' has been generated in the '"'+xml_path+''" directory.\n');

    // Emit C code through the P Toolset from the external path
    // ------------------------------------------------------------------------
    [macros,path]=libraryinfo("ga_xml_generatorlib");
    geneauto_root=fullpath(sprintf("%s../../external/geneauto/", path));
    geneauto_version="2.4.10"

    launcherjar=geneauto_root+"geneauto.galauncher-"+geneauto_version+".jar";
    if ~isfile(launcherjar) then
        error(launcherjar + " could not be found on your system.");
    end

    launcher="geneauto.launcher.GALauncherSCICOS";

    jarsep=":";
    if (getos() == "Windows") then jarsep=";"; end
    jars=strcat(ls(geneauto_root+"*.jar"), jarsep);

    cmd="java -Xmx1024m -Dgeneauto.home=''"+geneauto_root+"'' -cp ''"+jars+"'' "+launcher+" ''"+xml_file+"''";
    mprintf('Launching the GeneAuto toolchain\n');
    unix_w(cmd)
    mprintf('Exiting the GeneAuto toolchain\n');

    // FIXME: for Xcos we only need to store the block uuid ?
    ga_clear_context()
    
    if ~ok then clear FindSBParams;return,end
endfunction
