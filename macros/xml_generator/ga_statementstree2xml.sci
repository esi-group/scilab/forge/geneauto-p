function txt=ga_statementstree2xml(statementstree)
  // Author : Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  global id_num;
  txt=[]
  for t=statementstree
    if typeof(t)=='equal' then
      name=t.lhs
      expr=simplify_tree(t.expression)
      txt=[txt;
	   '<Variable_SM '+newxmlid()+' isConst=""true"" isOptimizable=""false"" isStatic=""false"" isVolatile=""true"" name=""'+name+'"">'
	   '    '+ga_expressiontree2xml(expr)
	   '<Variable_SM']
    end
  end
endfunction
