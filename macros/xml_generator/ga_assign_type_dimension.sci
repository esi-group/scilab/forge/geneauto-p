function IOPort=ga_assign_type_dimension(vv,nn,IOflag)
  // Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  FirstDimension=vv(IOflag)(nn)
  SecondDimension=vv(IOflag+'2')(nn)
  IOPort=['<dataType type=""gaxml:object"">']
  if  FirstDimension*SecondDimension==1 then
    IOPort=[IOPort;
	    '    '+give_type(vv,nn,IOflag)]
  elseif FirstDimension>1 & SecondDimension==1 then
    IOPort=[IOPort;
	    '    <TVector '+newxmlid()+'>'
	    '        <baseType type=""gaxml:object"">'
            '            '+give_type(vv,nn,IOflag)
	    '        </baseType>'
	    '        '+dimensions2xml(FirstDimension)
	    '    </TVector>' ]
  else
    IOPort=[IOPort;
	    '    <TMatrix '+newxmlid()+'>'
	    '        <baseType type=""gaxml:object"">'
	    '            '+give_type(vv,nn,IOflag)
	    '        </baseType>'
	    '        '+dimensions2xml([FirstDimension SecondDimension])
	    '    </TMatrix>']
  end
  IOPort=[IOPort;
	  '</dataType>'];
endfunction

function IOPort=give_type(vv,nn,IOflag)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  blocks_nb=['32','16','8','32','16','8']
  blocks_sg=['true','false']
  typ=IOflag+'typ'
  if vv(typ)(nn)==1 then
    IOPort='<TRealDouble '+newxmlid()+'/>'
  elseif vv(typ)(nn)>2&vv(typ)(nn)<9 then
    nbits=blocks_nb(vv(typ)(nn)-2)
    sg=blocks_sg(int(vv(typ)(nn)/6)+1)
    IOPort='<TRealInteger '+newxmlid()+' nBits=""'+nbits+'"" signed=""'+sg+'""/>'
  elseif vv(typ)(nn)==9 then
    IOPort='<TBoolean defaultInitialValue=""false"" '+newxmlid()+'/>'
  end
endfunction
