function [ok,freof]=ga_other_tests(cur_scs_m,freof)
// - Check if the system contains only identical sample clocks 
// - Checks if there is no blocks with continuous state
// - Check if there is no asynchronous clocks  
// Author : Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
    
  global  lk  ksel 
  ok=%t
  if argn(2)==1 then freof=[],end
  for k=1:size(cur_scs_m.objs)
    o=cur_scs_m.objs(k)
    if typeof(o)=='Block' then
      if o.gui=='SampleCLK' then
	if freof==[] then
	  freof=o.model.rpar
	  p1=[lk k]
	elseif or(freof<>o.model.rpar) then
      msg = ['SampleCLK with different sample time or offset '
		   'are not yet supported by GeneAuto'];
	  hilite_path([lk k], msg, %f);
	  hilite_path(p1, msg, %f);
	  message(msg)
	  ok=%f
	  return
	end
      elseif or(o.gui==['CLOCK_c','CLOCK_f']) then
	mess=['Asynchrous clocks are not supported by GeneAuto';
	      'they have to be substituted by  SampleCLK'];
	hilite_path([lk k], mess,%f);
	ok=%f
	return
      elseif or (o.model.sim(1)==["super","csuper","asuper"]) then
	lk=[lk k]
	[ok,freof]=ga_other_tests(o.model.rpar,freof)
	lk=lk(1:$-1)
	if ~ok then return,end
      elseif o.model.state<>[] then
        msg = ['Blocks with continuous state are not allowed'
		 'by GeneAuto'];
        hilite_path([lk k], msg, %f);
        message(msg);
        ok=%f
        return
      end
    end
  end
endfunction
