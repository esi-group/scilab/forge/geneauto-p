function ga_set_context(scs_m,k)
//used for error reporting into the scicos diagram
// Author : Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  global ksel scs_msav 
  ksel=k // number of the superblock to translate into scs_m
  scs_msav=scs_m //Scicos diagram that contains the superblock to translate
endfunction
	
	
