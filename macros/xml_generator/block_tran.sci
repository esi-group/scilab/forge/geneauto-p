function [name,labels,values,EXPRS,ok]=block_tran(o);
// Author : Fady Nassif, Copyright INRIA, GPL Version 3, 29 June 2007
   ierr=execstr('[name,labels,values,EXPRS]='+o.gui+'_tran(o)', ...
		'errcatch','n');
   if ierr<>0
     name=[];labels=[];values=list();EXPRS=[];ok=%f 
     return;
   else
       ok=%t
   end
endfunction
