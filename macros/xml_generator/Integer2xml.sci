// Convert integers to Gene-Auto XML
  
// Authors : Fady Nassif, Serge Steer. INRIA. 2007-2009. GPL Version 3
// Contributors : Daniel Tuulik. IB Krates. 2011

// Copyright (C) INRIA 2007-2009
// Copyright (C) IB Krates 2011

function [txt,ok]=Integer2xml(value)
  idstart=getxmlid()
  id=newxmlid()
  [xmls,ok]=scalardatatype2xml(value)
  if ~ok then txt=[];setxmlid(idstart),return,end
  txt=['<IntegerExpression hexValue=""false"" '+id+' integerPart=""1"" isNegative=""true""  litValue=""'+string(value)+'"">'
       '    '+xmls
       '</IntegerExpression>']
endfunction
