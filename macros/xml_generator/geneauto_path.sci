// Get the path of Gene-Auto toolset hierachy
  
// Authors : Fady Nassif, Serge Steer. INRIA. 29 June 2007. GPL Version 3
// Contributors : Daniel Tuulik. IB Krates. 08 April 2011

// Copyright (C) INRIA 2007-2009
// Copyright (C) IB Krates 2011

function path=geneauto_path() 
  	path=getenv("GENEAUTO_HOME", 'notpresent');
endfunction
