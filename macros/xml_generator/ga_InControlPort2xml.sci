function [txt,ports_association,ok]=ga_InControlPort2xml(model,blk_num)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  nports= size(model.evtin,'*')
  if nports>1 then
    message('A Subsystem cannot have more than one event input');
    ok=%f;txt=[];ports_association=[]
    return
  end
  ok=%t;
  if size(model.evtin,'*')>0 then
    port_id=getxmlid()
    txt=['<inControlPorts type=""gaxml:collection"">'
	 '    <InControlPort '+newxmlid()+' periodicSampleTime=""false"" resetStates=""false"" relatedToInportBlock=""false""/>'
	 '</inControlPorts>']
    ports_association=[blk_num 1 port_id 1 -1]
  else
    txt=[],ports_association=[]
  end
endfunction
