function [txt,ok]=vect2xml(value)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  idstart=getxmlid()
  txt=['<ListExpression '+newxmlid()+'>'
       '    <expressions type=""gaxml:collection"">']
  for k=1:size(value,2)
    [xmls,ok]=Scalar2xml(value(k))
    if ~ok then txt=[];setxmlid(idstart),return,end
    txt=[txt;
	 '        '+xmls]
  end

  txt=[txt
       '    </expressions>'
       '</ListExpression>']
endfunction
