// Convert unary operators to Gene-Auto XML
  
// Authors : Fady Nassif, Serge Steer. INRIA. 29 June 2007. GPL Version 3
// Contributors : Daniel Tuulik, Andres Toom. 08 April 2011

// Copyright (C) INRIA 2007-2009
// Copyright (C) IB Krates 2011

function [txt,ok]=unaryops2xml(operands,op)

  opstable=['+' 'ADD_OPERATOR' "0" "12"
	        '-' 'SUB_OPERATOR' "1" "12"]
  
  k=find(opstable(:,1)==op)
  if k==[] then
    txt=[];ok=%f
    errormessage('Operator ""'+operator+'"" is not yet supported")
  end
	    
  idstart=getxmlid()
  if k==1 then
    if typeof(operands(1))=="variable" then
      [txt,ok]=VariableExpression(operands(1).name)
    else
      [txt,ok]=ga_expressiontree2xml(operands(1))
    end
  else
    if typeof(operands(1))=="variable" then
      [txt,ok]=VariableExpression('-'+operands(1).name)
    elseif typeof(operands(1))=="cste"&size(operands(1).value,'*')==1 then
      [txt,ok]=Scalar2xml(-operands(1).value)
    else
      txt=[]; ok=%f;setxmlid(idstart)
      errormessage('Unary minus can only be applied to a variable name or a scalar constant')
      return
    end
  end
endfunction
