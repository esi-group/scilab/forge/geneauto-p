function [name,label,values,EXPRS]=SWITCH2_m_tran(o)
  values=list();
  name= 'Switch'
  label= ['Criteria','Threshold'];
  if o.model.rpar==[] then
     values(2)=o.model.opar(1)
  else
     values(2)=o.model.rpar;
  end
  OPER=['u2 >= Threshold','u2 > Threshold','u2 ~= 0'];
  values(1)=OPER(o.model.ipar+1);
  EXPRS=['',o.graphics.exprs($-1)]
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
endfunction
