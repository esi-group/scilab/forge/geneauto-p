function [name,label,values,EXPRS]=CONVERT_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  r=[8 13 18 23 28 33]
  i32=[2 14 19 29 34 38 56 65 83]
  ui32=[5 16 21 31 36 41 46 50 53 68 73 77 80];
  i16=[3 9 20 24 35 39 44 57 61 66 71 84 88]
  ui16=[6 11 22 26 37 42 47 51 54 59 69 74 78 81 86]
  i8=[4 10 15 25 30 40 45 49 58 62 64 67 72 76 85 89 91]
  ui8=[7 12 17 27 32 43 48 52 55 60 63 70 75 79 82 87 90]
  bool=92:96
  if or(o.model.ipar==r) then 
    v='double'
  elseif or(o.model.ipar==i32) then 
    v='int32'
  elseif or(o.model.ipar==ui32) then 
    v='uint32'
  elseif or(o.model.ipar==i16) then 
    v='int16'
  elseif or(o.model.ipar==ui16) then 
    v='uint16'
   elseif or(o.model.ipar==i8) then 
    v='int8'
  elseif or(o.model.ipar==ui8) then 
    v='uint8'
  elseif or(o.model.ipar==bool) then   
    v='boolean'
  else
    disp ici;disp(o.model.ipar)
    v='???'
  end
  r=[2:7 38:43 65:70]
  i32=[8:12 44:48 71:75]
  i16=[13:17 49:52 76:79];
  i8=[18:22 53:55 80:82]
  ui32=[23:27 56:60 83:87]
  ui16=[28:32 61:63 88:90]
  ui8=[33:37 64 91]
  bool=[1 92 96:98]
  if or(o.model.ipar==r) then 
    vv='double'
  elseif or(o.model.ipar==i32) then 
    vv='int32'
  elseif or(o.model.ipar==ui32) then 
    vv='uint32'
  elseif or(o.model.ipar==i16) then 
    vv='int16'
  elseif or(o.model.ipar==ui16) then 
    vv='uint16'
   elseif or(o.model.ipar==i8) then 
    vv='int8'
  elseif or(o.model.ipar==ui8) then 
    vv='uint8'
  elseif or(o.model.ipar==bool) then   
    vv='boolean'
  else
     disp la;disp(o.model.ipar)
    vv='???'
  end
  name='DataTypeConversion'
  label= ['DataType';'OutDataTypeMode'];
  values=list(vv,v)
  EXPRS=['';''];
endfunction
