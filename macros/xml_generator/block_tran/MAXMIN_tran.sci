function [name,label,values,EXPRS]=MAXMIN_tran(o)
  name='MinMax';
  label=['Function' "OutDataTypeMode" ];
  fn=['undefined';'min';'max']
  values=list(fn(o.model.ipar + 1),"Inherit via internal rule");
  EXPRS=['' ''];
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
endfunction
