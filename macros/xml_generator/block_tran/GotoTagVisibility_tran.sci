function [name,label,values,EXPRS]=GotoTagVisibility_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  name= o.gui
  label= 'GotoTag';
  values=list(o.model.opar(1))
  EXPRS=o.graphics.exprs
endfunction
