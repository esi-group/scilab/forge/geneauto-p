function [name,label,values,EXPRS]=LIM_tran(o)
  name='LIM';
  if o.model.ipar<>1 then label=[];values=list();EXPRS=[];
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  else
     label=['PARH1';'PARH2'];
     values=list(o.model.rpar(1),o.model.rpar(2))
     EXPRS=o.graphics.exprs
  end
endfunction
