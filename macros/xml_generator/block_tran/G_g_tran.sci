function [name,label,values,EXPRS]=G_g_tran(o)
  name='G';
  label=['EXTH1';'PARH1'];
  values=list('on',o.model.opar);
  EXPRS=['',o.graphics.exprs];
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
endfunction
