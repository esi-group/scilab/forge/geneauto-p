function [name,label,values,EXPRS]=CSCOPXY3D_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  name='Scope'
  label=[]
  values=list();EXPRS=o.graphics.exprs
endfunction
