function [name,label,values,EXPRS]=INPUTPORT_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29
// June 2007

  //This function is called only for the topvel inputports
    
  blocks_type=['double','double','int32','int16','int8','uint32','uint16','uint8','boolean']

  lnk=scs_m.objs(o.graphics.pout);
  //find the real link origin through splits
  to_block=lnk.to(1);
  to_port=lnk.to(2);
  o1=scs_m.objs(to_block);
  while (o1.model.sim(1)=='lsplit') do
    lnk=scs_m.objs(o1.graphics.pout(to_port));
    to_block=lnk.to(1);
    to_port=lnk.to(2);
    o1=scs_m.objs(to_block);
  end
  [vv,to_port]=inoutport(scs_m,to_block,to_port,corinv,lk,bllst,1);
  Outtyp=blocks_type(vv.intyp(to_port))
  szin=[vv.in(to_port) vv.in2(to_port)]
  
  
  name ='Inport'
  label=[   'DataType','PortDimensions','SignalType','Port','VectorParams1D']
  //The dimension must be a scalar (TTyper.TArray.addDimension )
  values=list(Outtyp,   prod(uint8(szin)),        'auto',string(o.model.ipar),'off')
//values=list(Outtyp,   int8(-1),        'auto',   string(o.model.ipar),'off')
  EXPRS=[       ''       ''                ''          ''             '']
endfunction
