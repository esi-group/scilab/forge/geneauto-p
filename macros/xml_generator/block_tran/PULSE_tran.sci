function [name,label,values,EXPRS]=PULSE_tran(o)
if size(o.model.ipar,'*')==1 then
  if o.model.ipar==1 then
    name='PULSE1';
  else
    name='PULSE2'
  end
  label=['EXTH1';'EXTH2'];
  values=list('off','off');
  EXPRS=[sci2exp(values(1)),sci2exp(values(2))];
else
  if o.model.ipar(3)==1 then
    name='PULSE1';
  else
    name='PULSE2'
  end 
  label=['EXTH1';'EXTH2';'PARH1';'PARH2'];
  values=list('on','on',o.model.ipar(1),o.model.ipar(2));
  bool=['false','true']
  o.graphics.exprs(1)=bool(eval(o.graphics.exprs(1))+1);
  o.graphics.exprs(2)=bool(eval(o.graphics.exprs(2))+1);
  EXPRS=['';'';o.graphics.exprs(1);o.graphics.exprs(2)];
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
end
endfunction
