function [name,label,values,EXPRS]=OUTPUTPORT_tran(o)
  // Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
name ='Outport'
label= [    'DataType',"InitialOutput" 'PortDimensions','OutputWhenDisabled','Port']
values=list('auto',      [],                 int8(-1),       'held',            string(o.model.ipar))
EXPRS=[      '',          ''                  ''               ''                '']
endfunction
