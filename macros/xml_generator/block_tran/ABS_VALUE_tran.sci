function [name,label,values,EXPRS]=ABS_VALUE_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  name='Abs';
  label='Value';
  values=list(evstr(o.graphics.exprs));EXPRS=o.graphics.exprs;
endfunction
