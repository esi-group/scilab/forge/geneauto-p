function [name,label,values,EXPRS]=CONST_m_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  name= 'Constant'
  if o.model.rpar<>[] then
     values=list(o.model.rpar,'Inherit from ''Constant value''','off')
  else
     values=o.model.opar;values($+1)='Inherit from ''Constant value''';values($+1)='off'
  end
  EXPRS=[o.graphics.exprs '' '']
  label=['Value';'OutDataTypeMode';'VectorParams1D']
endfunction
