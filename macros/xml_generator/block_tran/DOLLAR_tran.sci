function [name,label,values,EXPRS]=DOLLAR_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  name= 'UnitDelay'
  label='InitialValue'
  if o.model.dstate==[] then
     values=o.model.odstate
  else
     values= list(o.model.dstate)
  end
  EXPRS=o.graphics.exprs(1)
endfunction
