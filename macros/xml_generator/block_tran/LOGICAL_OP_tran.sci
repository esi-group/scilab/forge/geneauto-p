function [name,label,values,EXPRS]=LOGICAL_OP_tran(o)
  values=list()
  label=['Operator','Inputs']
  OPER=['AND','OR','NAND','NOR','XOR','NOT']
  values(1)=OPER(o.model.ipar(1)+1)
  values(2)=size(o.model.intyp,'*');
  if size(o.model.ipar,'*')==1 then
     name='Logic'
  else
    if o.model.ipar(2)==0 then
       name='Logic'
    else 
      name='BitwiseOperator'
    end
  end
  EXPRS=['',''];
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
endfunction
