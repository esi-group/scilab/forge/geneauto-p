function [name,label,values,EXPRS]=DEMUX_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  name= 'Demux'
  label=['Outputs','BusSelectionMode'];
  values=list(string(prod((o.model.ipar))),'off');
  EXPRS=['',''];
endfunction
