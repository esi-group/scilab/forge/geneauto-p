function [name,label,values,EXPRS]=IFTHEL_f_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
//The simulink equivalent of an If block has an parameter of the form
//u1>0, has u1 has here a special meaning (no type nor value) we
//suppose that the expression is '%if_u1>0', this special case will
//be handled by VariableExpression2xml

name='If';
label=['ShowElse';'IfExpression'];
if o.graphics.peout(2)<>0 then
  showelse='on'
else
 showelse='off'
end
values=list(showelse,'');
EXPRS=['','%if_u1>0'] //trick described above
endfunction
