function [name,label,values,EXPRS]=TrigFun_tran(o)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  name='Trigonometry'
  label= 'Operator';
  values=list(o.graphics.exprs)
  EXPRS=o.graphics.exprs;
endfunction
