function [name,label,values,EXPRS]=RELATIONALOP_tran(o)
  // Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007

  name= 'RelationalOperator'
  label='Function'
  OPER=['==','~=','<','<=','>','>=']
  values=list(OPER((o.model.ipar)+1))
  EXPRS=''
endfunction
