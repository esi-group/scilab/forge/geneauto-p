// Determine the path of the Gene-Auto launcher
  
// Authors : Fady Nassif, Serge Steer. INRIA. 29 June 2007. GPL Version 3
// Contributors : Daniel Tuulik. IB Krates. 08 April 2011

// Copyright (C) INRIA 2007-2009
// Copyright (C) IB Krates 2011

function launcherpath=ga_launcher_path()
  GeneAutoPath=getenv("GENEAUTO_HOME", 'notpresent') //geneauto_path();//get the path of GeneAuto toolset hierachy
  
  if GeneAutoPath=='notpresent' then 
    messagebox('Environment variable GENEAUTO_HOME not present! Please define to use Gene-Auto', "Geneauto", "error")
    error('Environment variable GENEAUTO_HOME not present! Please define to use Gene-Auto.')
    launcherpath=[];
    return;
  end
  
  launcherpath=listfiles(GeneAutoPath+'/geneauto.galauncher-*.jar')
  if launcherpath==[] then
    mprintf('No  geneauto.galauncher_*.jar file found in '+GeneAutoPath+'\n')
    return
  end
  if size(launcherpath,'*')<>1 then
    mprintf('Several  geneauto.galauncher_*.jar file found in '+GeneAutoPath+ '\n')
    launcherpath=[];
    return;
  end

  launcherpath='""'+pathconvert(launcherpath,%f,%t)+'""'
endfunction
