function [txt,ok]=ga_expressiontree2xml(tree)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  select typeof(tree)
  case "operation"
    [txt,ok]=Expression2xml(tree.operands,tree.operator)
  case "funcall" then
    [txt,ok]=CallExpression2xml(tree.rhs,tree.name,tree.lhsnb)
  case "variable" then
    [txt,ok]=VariableExpression2xml(tree.name)
  case "cste" then
    [txt,ok]=Scalar2xml(tree.value)
  else
    errormessage('The '+typeof(tree)+' tree component is not handled')
    txt=[]
    ok=%f
  end
endfunction
