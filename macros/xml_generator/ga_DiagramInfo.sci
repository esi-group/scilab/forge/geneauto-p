function txt=ga_DiagramInfo(Pos)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007  

  x=string(Pos(1))
  y=string(Pos(2))
  w=string(Pos(3)-Pos(2))
  h=+string(Pos(4)-Pos(2))
  txt=['<diagramInfo type=""gaxml:object"">'
       '    <DiagramInfo positionX=""'+x+'"" positionY=""'+y+'"" sizeX=""'+w+'"" sizeY=""'+h+'""/>'
       '</diagramInfo>']
endfunction

