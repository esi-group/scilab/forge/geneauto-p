function [txt,ports_association,ok]=ga_OutControlPorts2xml(nports,blk_num)
// Authors : Fady Nassif, Serge Steer, Copyright INRIA, GPL Version 3, 29 June 2007
  txt=[];ports_association=[];ok=%t;
  if nports>0 then
    txt='<outControlPorts type=""gaxml:collection"">'
    for i=1:nports
      port_id=getxmlid()
      txt=[txt;
	   '    <OutControlPort '+newxmlid()+' portNumber=""'+string(i)+'""/>']

      ports_association=[ports_association;
			 blk_num i port_id -1 -1]
    end
    txt=[txt;  
	    '</outControlPorts>']
  end
endfunction
