// This file is released under the 3-clause BSD license. See COPYING-BSD.

function buildmacros()
  macros_path = get_absolute_file_path("buildmacros.sce");
  tbx_build_macros("ga_blockslib", macros_path);

  // all sci files are blocks
  macros = ls(macros_path + "/*.sci");
  macros = strsubst(macros, macros_path + filesep(), "");
  macros = strsubst(macros, ".sci", "");

  tbx_build_blocks(toolbox_dir, macros', macros_path);
endfunction

buildmacros();
clear buildmacros; // remove buildmacros on stack

