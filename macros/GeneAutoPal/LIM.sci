function [x,y,typ]=LIM(job,arg1,arg2)
//
// Copyright INRIA
x=[];y=[];typ=[];
select job
case 'plot' then
  standard_draw(arg1)
case 'getinputs' then
  [x,y,typ]=standard_inputs(arg1)
case 'getoutputs' then
  [x,y,typ]=standard_outputs(arg1)
case 'getorigin' then
  [x,y]=standard_origin(arg1)
case 'set' then
  x=arg1
  model=x.model;
  graphics=x.graphics
  exprs=graphics.exprs
  num1=model.ipar
  [ln,fun]=where();
  if find (fun=="SUPER_f")<>[] then
    v=find (fun=="SUPER_f")
    fun=fun(1:v(1)-1)
  end
  if or(fun=="clickin") then
     num=x_choose(['internal';'external'],['Choose whether you want the Init and B_init';..
					  'to be parameters block or external input']);
  else
     num=num1
   end  
  if num==0 then return; end 
  while %t then 
  if num<>1 then
    model.ipar=num
    in=[1 1;1 1;1 1];it=[1 1 1]
    ok=%t;
    model.rpar=[];
  else
    model.ipar=num
    [ok,minim,maxim,exprs]=getvalue('Set LIM parameter',..
	    ['Minimum';'Maximum'],list('vec',1,'vec',1),exprs)
    if ~ok then break,end
    if minim>maxim then message ('Maximum must be greater than Minimum');ok=%f;end
    in=[1 1];it=1
    model.rpar=[minim;maxim]
  end
  if ok then
      out=[1 1];ot=1
     [model,graphics,ok]=set_io(model,graphics,list(in,it),list(out,ot),[],[])
     graphics.exprs=exprs
     x.model=model;x.graphics=graphics;
     arg1=x;
     break;
   end
  end
case 'define' then
  model=scicos_model()
   minim=0;maxim=1
  junction_name='lim';
  funtyp=4;
  model.sim=list(junction_name,funtyp)
  model.in=[1;1;1]
  model.in2=[1;1;1]
  model.intyp=[1 1 1]
  model.out=1
  model.out2=1
  model.outtyp=1
  model.evtin=[]
  model.evtout=[]
  model.state=[]
  model.dstate=[]
  model.ipar=2
  model.rpar=[minim;maxim]
  model.blocktype='c' 
  model.firing=[]
  model.dep_ut=[%t %f]
  exprs=[sci2exp(minim);sci2exp(maxim)]
  gr_i=['[x,y,typ]=standard_inputs(o) ';
	'dd=sz(1)/10,de=9*sz(1)/10';
	'if size(y,''*'')==3 then ';
	'xstring(orig(1)+dd/2,y(1)-4,''E'')';
	'xstring(orig(1)+dd/2,y(2)-4,''MIN'')';
	'xstring(orig(1)+dd/2,y(3)-4,''MAX'')';
	'else ';
	'xstring(orig(1)+dd/2,y(1)-4,''E'')';
	'end ';
	'[x,y,typ]=standard_outputs(o) ';
	'xstring(orig(1)+sz(1)-dd,y(1)-4,''S'')';
	'xstringb(orig(1)+sz(1)/3,orig(2),''LIM'',2*sz(1)/3-sz(1)/10,sz(2)*9/10,''fill'')';
        'x=orig(1)*ones(4,1)+sz(1)/3*[0;1/4;3/4;1]+sz(1)/5';
        'y=orig(2)*ones(4,1)+(4/5)*sz(2)*[0;0;1;1]+sz(2)/10';
	'xpoly(x,y)']
  x=standard_define([5 3],model,exprs,gr_i)
end
endfunction

