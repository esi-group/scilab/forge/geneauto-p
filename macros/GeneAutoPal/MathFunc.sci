function [x,y,typ]=MathFunc(job,arg1,arg2)
// Copyright INRIA
x=[];y=[];typ=[];
select job
case 'plot' then
    num1=arg1.model.ipar
    txta=['e^u';'ln';'10^u';'log10';'|u|^2';'u^2';'sqrt';'u^v';'conj';'1/u';..
         'hypot';'rem';'mod';'u''';'hermitian']
    txt=txta(num1);
  standard_draw(arg1)
case 'getinputs' then
  [x,y,typ]=standard_inputs(arg1)
case 'getoutputs' then
  [x,y,typ]=standard_outputs(arg1)
case 'getorigin' then
  [x,y]=standard_origin(arg1)
case 'set' then
  x=arg1;
  graphics=arg1.graphics;exprs=graphics.exprs
  model=arg1.model;
  num1=model.ipar;
  [ln,fun]=where();
  if or(fun=="xcosBlockInterface") then
  num=x_choose(['exp';'log';'10^u';'log10';'magnitude^2';..
                    'square';'sqrt';'pow';'conj';'reciprocal';..
                    'hypot';'rem';'mod';'transpose';'hermitian'],'Function')
  else
  num=num1
  end
  if num==0 then return; end
  model.ipar=num
  while %t do
    [ok,dtype,inh,exprs]=getvalue('Set block parameters',..
	['Datatype';'Sample time Inherit (no:0, yes:1)'],...
			      list('vec',1,'vec',1),exprs(1:2))
    if ~ok then break,end
    if (inh<>0 & inh<>1) then message('Invalid Inherit parameter');ok=%f;end
    if num==8|num==11|num==12|num==13 then
       in=[-1 -2;-1 -2]
       if num==11 & dtype<>1 then 
          message ('Real signal is only supported; Datatype must be 1');ok=%f;
       end
    else
       in=[-1 -2];
    end
    if num==14|num==15 then
       out=[-2 -1]
    else
       out=[-1 -2]
    end
    if (num<>12&num<>13) then
    if (dtype<1|dtype>2) then 
         message('Datatype is not supported; must be 1 or 2');ok=%f;
    end
    else
    if (dtype<1|dtype>8) then 
         message('Datatype is not supported; must be 1 to 8');ok=%f;
    end
    end
    it=dtype*ones(1,size(in,1));
    ot=dtype;
    simd=['expblk_m';'logblk_m';'expblk_m';'logblk_m';'magn';'powblk_m';..
          'mat_sqrt';'powblk_m';'matz_conj';'invblk_m';'hypot_d';'remains';..
          'modulos';'mattran_m';'mattran_m'];
    simz=['expblkz_m';'logblkz_m';'expblkz_m';'logblkz_m';'magn';..
          'powblkz_m';'matz_sqrt';'powblkz_m';'matz_conj';'invblkz_m';..
          'hypot_d';'remains';'modulos';'matztran_m';'mathermit_m'];
    if num==1|num==2 model.rpar=%e
    elseif num==3|num==4 then model.rpar=10
    elseif num==6 then model.opar=list(2,0);
    else model.rpar=[]
    end
    if dtype==1 then model.sim=list(simd(num),4)
    else model.sim=list(simz(num),4)
    end
    if ok then
      [model,graphics,ok]=set_io(model,graphics,list(in,it),list(out,ot),ones(1-inh,1),[])
    end
    if ok then
      graphics.exprs=exprs;
      x.graphics=graphics;x.model=model
      break
    end
  end

case 'define' then
  model=scicos_model()
  model.sim=list('expblk_m',4)
  model.in=-1
  model.in2=-2
  model.intyp=1
  model.out=-1
  model.out2=-2
  model.outtyp=1
  model.evtin=[]
  model.dstate=[]
  model.blocktype='d'
  model.dep_ut=[%t %f]
  model.ipar=1
  model.rpar=%e
  exprs=[sci2exp(1);sci2exp(1)]

  gr_i='xstringb(orig(1),orig(2),txt,sz(1),sz(2),''fill'')'
  x=standard_define([2 2],model,exprs,gr_i)
end
endfunction