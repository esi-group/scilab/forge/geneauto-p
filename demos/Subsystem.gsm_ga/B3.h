
#ifndef __B3__
#define __B3__

/* Includes */

#include "GATypes.h"
#include "Subsystem_types.h"
#include "B3_6.h"
#include "B3_7.h"


/* Variable Declarations */

extern REAL B3_B3_6_B3_6_3;
extern REAL B3_B3_7_B3_7_4;
extern t_B3_7_io _B3_7_io;
extern t_B3_6_io _B3_6_io;

/* Function prototypes */

extern void B3_init(t_Subsystem_state *_state_);
extern void B3_compute(t_B3_io *_io_, t_Subsystem_state *_state_);


#endif
