
#ifndef __B3_6__
#define __B3_6__

/* Includes */

#include "GATypes.h"
#include "Subsystem_types.h"


/* Function prototypes */

extern void B3_6_init(t_Subsystem_state *_state_);
extern void B3_6_compute(t_B3_6_io *_io_, t_Subsystem_state *_state_);


#endif
